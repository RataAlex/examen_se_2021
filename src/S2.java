import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.io.*;

class S2 extends JFrame{



    JLabel Text1;
    JTextField text1;
    JTextArea text2;
    JButton transfer;

    S2(){

        setTitle("File_read");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400,600);
        setVisible(true);
    }

    public void init(){

        this.setLayout(null);
        int width=80;int height = 20;

        Text1 = new JLabel("Insert file name ");
        Text1.setBounds(10, 50, 120, height);



        text1 = new JTextField();
        text1.setBounds(130,50,240, height);



        transfer = new JButton("Press Me");
        transfer.setBounds(150,100,100, 30);

        transfer.addActionListener(new Teleport_text());

        text2 = new JTextArea();
        text2.setBounds(10,180,365,410);

        add(Text1);add(text1);add(transfer);add(text2);

    }

    public static void main(String[] args)
    {

        new S2();

    }

    class Teleport_text implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            String filename = S2.this.text1.getText();

            File file = new File("D:\\SE\\examen_se_2021\\"+filename);

            FileInputStream fileStream = null;
            try {
                fileStream = new FileInputStream(file);
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }

            InputStreamReader input = new InputStreamReader(fileStream);

            BufferedReader reader = new BufferedReader(input);

            String line="";

            while(true)
            {

                try {
                    if (!((line = reader.readLine()) != null)) break;
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
                S2.this.text2.append(line+"\n");
            }




        }
    }
}
